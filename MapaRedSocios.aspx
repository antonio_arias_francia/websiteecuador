﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="MapaRedSocios.aspx.cs" Inherits="SantaNaturaNetworkV3.MapaRedSocios" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/proyecto2/tienda.css" rel="stylesheet" />
    <%--<link href="css/proyecto2/vendors/elegant-icon/style.css" rel="stylesheet" />--%>
    <link href="css/AdminLTE-v1.css?v1" rel="stylesheet" type="text/css" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="css/tree-table/jquery.treegrid.css?v2" rel="stylesheet" type="text/css" />
    <style>
        .content-table {
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            min-width: 400px;
            border-radius: 5px 5px 0 0;
            overflow: hidden;
            box-shadow: 0px 5px 5px 0px lightgrey;
        }

            .content-table thead tr {
                background-color: var(--Medium-primary);
                color: #ffffff;
                text-align: left;
                font-weight: bold;
                font-size: 18px;
            }

            .content-table thead tr th label{
                margin-bottom: 0;
            }

            .content-table th, .content-table td {
                padding: 12px 15px !important;
            }

            .content-table tbody tr {
                border-bottom: 1px solid #dddddd;
            }

                .content-table tbody tr:nth-of-type(even) {
                }

                .content-table tbody tr:last-of-type {
                    /*border-bottom: 2px solid #009897;*/
                }

                .content-table tbody tr.active-row {
                    font-weight: bold;
                    color: #009897;
                }

        .titulo {
            font-weight: bold;
            text-align: start;
            margin: 0 82px;
            height: 100px;
            display: flex;
            align-items: center;
            color: var(--Dark-primary);
            font-family: roboto;
            font-size: 25px;
        }

        .bloqueFiltrado {
            border: 1.5px solid rgba(126, 224, 255, 0.47);
            display: flex;
            justify-content: space-between;
            margin: 0 82px;
            padding: 16px 19px 16px 53px;       
            border-radius: 30px;
            box-shadow: 0px 5px 5px 0px lightgrey;
        }

        .datosUsuario {
            display: flex;
            flex-direction: column;
        }

            .datosUsuario p {
                font-size: 22px;
                margin: 0;
                color: var(--Dark-primary);
                font-family: roboto;
                line-height: 1.2;
                display: flex;
                justify-content: space-between;
                grid-gap: 12px;
            }

        .filtrar {
            display: flex;
            align-items: center;
            grid-gap: 19px;
        }

        .combito select {
            /* Reset Select */
            appearance: none;
            outline: 0;
            border: 1px solid lightgray;
            border-radius: 6px;
            box-shadow: none;
            /* Personalize */
            flex: 1;
            padding: 0 1em;
            color: black;
            background-color: white;
            background-image: none;
            cursor: pointer;                        
            font-size: 19px;
            font-family: 'Quicksand', sans-serif;
        }
            /* Remove IE arrow */
            .combito select::-ms-expand {
                display: none;
            }


        .combito {
            position: relative;
            display: flex;
            width: 20em;
            height: 69px;
            border-radius: .25em;
            overflow: hidden;
        }

            /* Arrow */
            .combito::after {
                content: '\25BC';
                position: absolute;
                top: 0;
                right: 0;
                padding: 1.5em 1em;
                background-color: var(--Dark-primary);
                transition: .25s all ease;
                pointer-events: none;
                color: white;
                border-radius: 0 6px 6px 0;
                height: 69px;
                width: 60px;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .combito:hover::after {
                background-color: var(--Medium-primary);
            }

        .botonFiltrar {
            background-color: var(--Darkest-primary);
            transition: all .3s;
            border-radius: 25px;
            height: 48px;
            width: 166px;
            font-size: 19px;
            border: none;
            color: white;
            font-family: roboto;
        }

        .botonFiltrar:hover{
            background-color: var(--Medium-primary);            
        }


        .lblMedioDePago {
            margin-left: auto;
        }

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(img/loadingPageSantanatura.gif) center no-repeat #fff;
        }

        .style-button-red {
            background-color: transparent !important;
            color: black !important;
            border-color: transparent !important;
        }

        div.centerTable {
            text-align: center;
        }

            div.centerTable table {
                margin: 0 auto;
                text-align: left;
                margin-bottom: 70px;
            }

        @media (max-width: 500px) {
            .titulo{
                font-size: 26px
            }

            .bloqueFiltrado{
                grid-gap: 20px;
            }

            .datosUsuario p{
                font-size: 16px
            }

            .combito{
                width: 16em;
            }

            .combito select{
                font-size: small;
            }

            .bloqueFiltrado, .filtrar{
                flex-direction: column;
            }
        }

        .box{
            box-shadow: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-top: 70px">
        <p class="titulo">MAPA DE RED</p>
        <div id="page_loader" style="display: none" class="se-pre-con"></div>
        <div class="bloqueFiltrado">
            <div class="datosUsuario">
                <p>Total socios: <strong>0</strong></p>
                <p>Socios activos: <strong>0</strong></p>
                <p>Nuevos activos: <strong>0</strong></p>
                <p>Socios inactivos: <strong>0</strong></p>
            </div>
            <div id="Div1" class="filtrar">
                <div class="combito">
                    <asp:DropDownList ID="cboPeriodo" CssClass="ddlMapaDeRed" Width="100%" runat="server">
                    </asp:DropDownList>
                </div>
                <div style="display: flex; align-items: center;">
                    <button class="botonFiltrar" type="button" id="btnFiltro" style="outline: none;">FILTRAR</button>
                </div>
            </div>
        </div>

        <div>
            <br />
            <div class="box-body">
                <div class="row" style="width: 100%; margin: 0;">
                    <div class="col-md-12 centerTable" style="padding: 0;">
                        <div class="box box-success table-responsive" style="border: none;">
                            <div class="box box-header" style="border: none;">
                            </div>
                            <div style="margin: 0 82px;">
                                <table id="tbl_red" class="tree content-table table-bordered table-hover text-center table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">
                                                <label style="width: 100px;">Nivel</label></th>
                                            <th style="text-align: center;">Nombres</th>
                                            <th style="text-align: center; display: none;">Corazones</th>
                                            <th style="text-align: center;">PP</th>
                                            <th style="text-align: center;">VIP</th>
                                            <th style="text-align: center;">VP</th>
                                            <th style="text-align: center;">VR</th>
                                            <th style="text-align: center;">VG</th>
                                            <th style="text-align: center;">VQ</th>
                                            <th style="text-align: center;">Rango Actual</th>
                                            <th style="text-align: center;">Rango Máximo</th>
                                            <th style="text-align: center;">Inscripción</th>
                                            <th style="text-align: center;">Teléfono</th>
                                            <th style="text-align: center;">País</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <br />
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/tree-table/jquery.cookie.js?v2"></script>
    <script src="js/proyecto2/jqueryDataTablesPremioSocios.js"></script>
    <script src="js/proyecto2/estiloTablasPremioSocios.js"></script>
    <script src="js/tree-table/jquery.treegrid.js"></script>
    <script src="js/tree-table/jquery.treegrid.bootstrap3.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="js/sweetAlert.js" type="text/javascript"> </script>
    <script src="js/EstructuraRed.js?v9" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tree').treegrid({
                expanderExpandedClass: 'glyphicon glyphicon-minus',
                    expanderCollapsedClass: 'glyphicon glyphicon-plus',
                'saveState': true,
                'saveStateMethod': 'cookie',
                'saveStateName': 'tree-grid-state'
            })
        });
            window.onload = function () {
                document.getElementById("idMenuRed").style.color = 'white';
                document.getElementById("idMenuRed").style.borderBottom = '3px solid white';

                document.getElementById("idSubMenuMapaDeRed").style.background = '#2D4A9D';
            };
    </script>
</asp:Content>
